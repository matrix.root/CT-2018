#include <iostream>
#include <climits>

#define TASK "rmq2"

using namespace std;

const int N = static_cast<const int>(5e5 + 5);

int n;
long long a[N];

bool setted[N * 4];
long long value[N * 4];
long long sum[N * 4];

long long build(int v = 0, int begin = 0, int end = n) {
    setted[v] = false;
    sum[v] = 0;
    if (end - begin > 1) {
        int mid = (begin + end) / 2;
        return value[v] = min(
                build(v * 2 + 1, begin, mid),
                build(v * 2 + 2, mid, end));
    } else {
        return value[v] = a[begin];
    }
}

void push(int v) {
    if (setted[v]) {
        setted[v * 2 + 1] = setted[v * 2 + 2] = true;
        sum[v * 2 + 1] = sum[v * 2 + 2] = 0;
        value[v * 2 + 1] = value[v * 2 + 2] = value[v];
        setted[v] = false;
    }
    sum[v * 2 + 1] += sum[v];
    sum[v * 2 + 2] += sum[v];
    sum[v] = 0;
}

long long getMin(int begin, int end, int v = 0, int _begin = 0, int _end = n) {
    if (begin >= end) {
        return LLONG_MAX;
    } else if (begin == _begin && end == _end) {
        return value[v] + sum[v];
    } else {
        push(v);
        int _mid = (_begin + _end) / 2;
        value[v] = min(
                getMin(_begin, _mid, v * 2 + 1, _begin, _mid),
                getMin(_mid, _end, v * 2 + 2, _mid, _end)
        );
        return min(
                getMin(begin, min(end, _mid), v * 2 + 1, _begin, _mid),
                getMin(max(begin, _mid), end, v * 2 + 2, _mid, _end));
    }
}

long long setElements(int begin, int end, long long x, int v = 0, int _begin = 0, int _end = n) {
    if (begin >= end) {
        return value[v] + sum[v];
    } else if (begin == _begin && end == _end) {
        setted[v] = true;
        sum[v] = 0;
        return value[v] = x;
    } else {
        push(v);
        int _mid = (_begin + _end) / 2;
        return value[v] = min(
                setElements(begin, min(end, _mid), x, v * 2 + 1, _begin, _mid),
                setElements(max(begin, _mid), end, x, v * 2 + 2, _mid, _end));
    }
}

long long add(int begin, int end, long long x, int v = 0, int _begin = 0, int _end = n) {
    if (begin >= end) {
        return value[v] + sum[v];
    } else if (begin == _begin && end == _end) {
        return value[v] + (sum[v] += x);
    } else {
        push(v);
        int _mid = (_begin + _end) / 2;;
        return value[v] = min(
                add(begin, min(end, _mid), x, v * 2 + 1, _begin, _mid),
                add(max(begin, _mid), end, x, v * 2 + 2, _mid, _end));
    }
}

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    build();

    string cmd;
    int i, j, x;
    while (cin >> cmd >> i >> j) {
        if (cmd == "min") {
            cout << getMin(i - 1, j) << '\n';
        } else {
            cin >> x;
            if (cmd == "set") {
                setElements(i - 1, j, x);
            }
            if (cmd == "add") {
                add(i - 1, j, x);
            }
        }
    }

    return 0;
}