#include <iostream>
#include <vector>

#define TASK "sparse"

using namespace std;

const int N = static_cast<const int>(1e5 + 5);

int getNext(int i) {
    return (23 * i + 21563) % 16714589;
}

struct log2 {
    void init(int n) {
        data.assign(n, 0);
        for (int i = 1; i < n; ++i) {
            data[i] = data[(i - 1) / 2] + 1;
#if ON_MY_PC
//            fprintf(stderr, "log(%i) = %i\n", i, get(i));
#endif
        }
    }

    int get(int x) {
        return data[x - 1];
    }

    // data[i - 1] = log base 2 to of i
    vector<int> data;
};

struct SparseTable {
    SparseTable(vector<int> a) {
        n = a.size();
        logData.init(n);
#if ON_MY_PC
        fprintf(stderr, "sparse n = %i\n", n);
#endif

        data.resize(log(n) + 1);
        for (int i = 0; i < data.size(); ++i) {
            int len = (1 << i);
            data[i].resize(n - (len - 1));
        }

        data[0] = a;
        for (int i = 1; i < data.size(); ++i) {
            int len = (1 << (i - 1));
            for (int j = 0; j < data[i].size(); ++j) {
                data[i][j] = min(data[i - 1][j], data[i - 1][j + len]);
            }
        }

#if ON_MY_PC
        for (int i = 0; i < data.size(); ++i) {
            fprintf(stderr, "\nlength is %i\n", (1 << i));
            for (int j = 0; j < data[i].size(); ++j) {
                fprintf(stderr, "\tmin between [%i, %i) is %i\n", j, j + (1 << i), data[i][j]);
            }
        }
        fprintf(stderr, "sparse built\n\n");
#endif
    }

    int getAns(int u, int v) {
        return findMin(min(u, v) - 1, max(u, v));
    }

    int findMin(int begin, int end) {
        int len = log(end - begin);
        return min(data[len][begin], data[len][end - (1 << len)]);
    }
    // data[begin][len] = min[begin, begin + len - 1]

    int log(int n) {
        return logData.get(n);
    }

    int n;
    vector<vector<int>> data;
    log2 logData;
};

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    int n, m, a1, u, v, ans;
    cin >> n >> m >> a1 >> u >> v;

    vector<int> a(n, a1);
    for (int i = 1; i < n; ++i) {
        a[i] = getNext(a[i - 1]);
    }
#if ON_MY_PC
    for (int i = 0; i < a.size(); ++i) {
        fprintf(stderr, "a[%i] \t= %i\n", i, a[i]);
    }
    fprintf(stderr, "a[] built\n\n");
#endif

    SparseTable table(a);

    for (int i = 1; i < m; ++i) {
        ans = table.getAns(u, v);
#if ON_MY_PC
        fprintf(stderr, "min between [%i, %i] is %i\n", u - 1, v - 1, ans);
#endif
        u = ((17 * u + 751 + ans + 2 * i) % n) + 1;
        v = ((13 * v + 593 + ans + 5 * i) % n) + 1;
    }
    printf("%i %i %i\n", u, v, table.getAns(u, v));

    return 0;
}