#include <iostream>
#include <vector>
#include <iomanip>

#define TASK "rmq"

using namespace std;

unsigned n, m;

template<typename T>
class Request {
    size_t begin, end;
    T value;

public:
    void getFromCin() {
        cin >> begin >> end >> value;
        --begin;
    }

    size_t getBegin() {
        return begin;
    }

    size_t getEnd() {
        return end;
    }

    T getValue() {
        return value;
    }

    void print() {
#if ON_MY_PC
        fprintf(stderr, "REQUEST: [%3zu, %3zu] = %d\n", begin, end, value);
#endif
    }
};

template<typename T>
class Tree {
    size_t begin{}, end{};
    Tree<T> *lChild;
    Tree<T> *rChild;
    T value;
    bool updated;

public:
    size_t size() {
        return end - begin;
    }

    explicit Tree(size_t begin = 0, size_t end = n) {
        this->begin = begin;
        this->end = end;
        value = INT32_MIN;
        updated = false;
        if (size() > 1) {
            size_t mid = (begin + end) / 2;
            lChild = new Tree(begin, mid);
            rChild = new Tree(mid, end);
        }
    }

    void print(size_t depth = 0, size_t level = 0) {
#if ON_MY_PCx
        fprintf(stderr, "%stree[%3zu, %3zu] = %d ",
                string(level, '\t').c_str(), begin, end, value
        );
        if (updated) {
            fprintf(stderr, "UPDATED ");
        }
        if (size() > 1 && depth > 0 && !updated) {
            fprintf(stderr, "{\n");
            lChild->print(depth - 1, level + 1);
            rChild->print(depth - 1, level + 1);
            fprintf(stderr, "%s}", string(level, '\t').c_str());
        }
        fprintf(stderr, "\n");
#endif
    }

    T update(T value) {
        updated = true;
        return this->value = max(value, this->value);
    }

    void push() {
        if (updated) {
            lChild->update(this->value);
            rChild->update(this->value);
            updated = false;
        }
    }

    T update(size_t begin, size_t end, T value) {
        if (begin >= end) {
            return this->value;
        }
        if (begin == this->begin && end == this->end) {
            return update(value);
        }
        push();
        size_t mid = (this->begin + this->end) / 2;
        return this->value = min(
                lChild->update(begin, min(mid, end), value),
                rChild->update(max(mid, begin), end, value)
        );
    }

    T find(size_t begin, size_t end) {
        if (begin >= end) {
            return INT32_MAX;
        }
        if (begin == this->begin && end == this->end) {
            return value;
        }
        push();
        size_t mid = (this->begin + this->end) / 2;
        return min(
                lChild->find(begin, min(mid, end)),
                rChild->find(max(mid, begin), end)
        );
    }

    void printElements() {
        if (size() > 1) {
            push();
            lChild->printElements();
            rChild->printElements();
        } else {
            cout << value << ' ';
        }
    }
};

int main() {
#if ON_MY_PC
    time_t startTime = clock();
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    cin >> n >> m;
    Tree<int> tree;
    tree.print(5);
    vector<Request<int>> requests(m);
    for (auto &request : requests) {
        request.getFromCin();
    }
    for (auto request : requests) {
        tree.update(request.getBegin(), request.getEnd(), request.getValue());
        request.print();
        tree.print(5);
    }
    for (auto request : requests) {
        if (tree.find(request.getBegin(), request.getEnd()) != request.getValue()) {
            printf("inconsistent\n");
            return 0;
        }
    }
    cout << "consistent\n";
    tree.printElements();

#if ON_MY_PC
    fprintf(stderr, "Runtime: %5li", clock() - startTime);
#endif
    return 0;
}