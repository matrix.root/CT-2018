#include <iostream>

using namespace std;

template<typename M, typename S>
class Tree {
    const int begin, end;
    Tree *lChild, *rChild;
    M maximum = 0;
    S sum = 0;
    M newValue = 0;
    bool updated = true;

public:
    Tree(int begin, int end) : begin(begin), end(end) {
        lChild = rChild = nullptr;
    }

    void push() {
        if (lChild == nullptr) {
            int mid = (begin + end) / 2;
            lChild = new Tree(begin, mid);
            rChild = new Tree(mid, end);
        }
        if (updated) {
            lChild->update(newValue);
            rChild->update(newValue);
            newValue = 0;
            updated = false;
        }
    }

    void update(const M &newVal) {
        newValue = newVal;
        sum = maximum = newValue * size();
        updated = true;
    }

    void update(int begin, int end, const M &newVal) {
        if (begin >= end) {
            return;
        } else if (this->begin == begin && this->end == end) {
            update(newVal);
        } else {
            push();
            lChild->update(begin, min(lChild->end, end), newVal);
            rChild->update(max(rChild->begin, begin), end, newVal);
            sum = lChild->sum + rChild->sum;
            maximum = max((S) lChild->maximum, lChild->sum + rChild->maximum);
        }
    }

    int find(const M &height) {
        if (size() > 1) {
            push();
            if (lChild->maximum > height) {
                return lChild->find(height);
            } else {
                return rChild->find(height - lChild->sum);
            }
        }
        return maximum > height ? begin - 1 : begin;
    }

    int size() const {
        return end - begin;
    };

    M getMax() {
        return maximum;
    }
};

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    int n;
    cin >> n;
    Tree<int, long long> tree(0, n);
    char cmd;
    cin >> cmd;
    while (cmd != 'E') {
        switch (cmd) {
            case 'I':
                int a, b, d;
                cin >> a >> b >> d;
                tree.update(a - 1, b, d);
                break;
            case 'Q':
                int h;
                cin >> h;
                cout << tree.find(h) + 1 << '\n';
                break;
        }
        cin >> cmd;
    }

    return 0;
}