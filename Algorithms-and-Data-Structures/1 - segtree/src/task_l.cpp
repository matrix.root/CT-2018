#include <iostream>

#define TASK "parking"

using namespace std;

enum event {
    ENTER, EXIT
};

struct Request {
    event type;
    int x;

    Request get() {
        string s;
        cin >> s >> x;
        --x;
        if (s == "enter") {
            type = ENTER;
        } else {
            type = EXIT;
        }
        return *this;
    }
} request;

struct SegTree {
    int begin;
    int end;
    SegTree *lChild = nullptr;
    SegTree *rChild = nullptr;
    int val;

    SegTree(int begin, int end) {
        this->begin = begin;
        this->end = end;
        build();
    }

    int build() {
        if (end - begin == 1) {
            return val = begin;
        } else if (end - begin > 1) {
            int mid = (begin + end) / 2;
            return val = min((lChild = new SegTree(begin, mid))->val, (rChild = new SegTree(mid, end))->val);
        }
    }

    void handle(Request request) {
        switch (request.type) {
            case EXIT:
                setVal(request.x, request.x);
                break;
            case ENTER:
                auto result = findMin(request.x, end);
                if (result == INT32_MAX) {
                    result = findMin(begin, request.x);
                }
                setVal(result, INT32_MAX);
                cout << result + 1 << '\n';
        }
    }

    int findMin(int from, int to) {
        if (from >= to) {
            return INT32_MAX;
        } else if (from == begin && to == end) {
            return val;
        } else {
            int mid = (begin + end) / 2;
            return min(lChild->findMin(from, min(to, mid)), rChild->findMin(max(from, mid), to));
        }
    }

    int setVal(int x, int newVal) {
        if (end - begin == 1) {
            return val = newVal;
        } else {
            if (x < lChild->end) {
                return val = min(lChild->setVal(x, newVal), rChild->val);
            } else {
                return val = min(lChild->val, rChild->setVal(x, newVal));
            }
        }
    }
};

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    int n, m;
    cin >> n >> m;
    SegTree parking(0, n);
    for (int i = 0; i < m; ++i) {
        parking.handle(request.get());
    }

    return 0;
}