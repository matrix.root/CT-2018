#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

template<typename T>
class Treap {
    struct Node {
        Node *lChild, *rChild;
        T y, index;
        int size;

        explicit Node(T index) : y(index), size(1), index(index), lChild(nullptr), rChild(nullptr) {}

        void update() {
            size = 1 + getSize(lChild) + getSize(rChild);
        }

        static int getSize(Node *p) {
            return p == nullptr ? 0 : p->size;
        }

        static pair<Node *, Node *> split(Node *t, int count) {
            if (t == nullptr) {
                return pair<Node *, Node *>(nullptr, nullptr);
            }
            if (getSize(t->lChild) >= count) {
                auto result = split(t->lChild, count);
                t->lChild = result.second;
                t->update();
                return pair<Node *, Node *>(result.first, t);
            } else {
                auto result = split(t->rChild, count - getSize(t->lChild) - 1);
                t->rChild = result.first;
                t->update();
                return pair<Node *, Node *>(t, result.second);
            }
        }

        static Node *merge(Node *first, Node *second) {
            if (second == nullptr) {
                return first;
            } else if (first == nullptr) {
                return second;
            } else if (first->y > second->y){
                first->rChild = merge(first->rChild, second);
                first->update();
                return first;
            } else {
                second->lChild = merge(first, second->lChild);
                second->update();
                return second;
            }
        }

        static void print(Node *root) {
            if (root == nullptr) {
                return;
            }
            print(root->lChild);
            printf("%i ", root->index);
            print(root->rChild);
        }
    } *root;

public:
    explicit Treap(int n) {
        root = nullptr;
        for (T i = 1; i <= n; ++i) {
            root = Node::merge(root, new Node(i));
        }
    }

    void moveForward(int leftBorder, int rightBorder) {
        auto shared = Node::split(root, rightBorder);
        Node *right = shared.second;
        shared = Node::split(shared.first, leftBorder - 1);
        right = Node::merge(shared.first, right);
        root = Node::merge(shared.second, right);
    }

    void print() {
        Node::print(root);
    }
};

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    int n, m;
    cin >> n >> m;
    Treap<int> treap(n);
    for (int i = 0; i < m; ++i) {
        int begin, end;
        cin >> begin >> end;
        treap.moveForward(begin, end);
    }
    treap.print();

    return 0;
}