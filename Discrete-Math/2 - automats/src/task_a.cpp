#include <iostream>
#include <vector>

#define TASK "problem1"

using namespace std;

string word;
vector<pair<bool, vector<pair<int, char>>>> conditions;

void accepts() {
    printf("Accepts\n");
    exit(0);
}

void doCondition(int conditionIndex, int traversed = 0) {
    if (traversed == word.size() && conditions[conditionIndex].first) {
        accepts();
    }
    
    auto &index = traversed;
    for (auto transition : conditions[conditionIndex].second) {
        if (word[index] == transition.second) {
            doCondition(transition.first, traversed + 1);
        }
    }
}

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    int n, m, k;
    cin >> word >> n >> m >> k;
    conditions.resize(static_cast<unsigned long>(n));
    for (int i = 0; i < k; ++i) {
        int number;
        cin >> number;
        conditions[--number].first = true;
    }
    for (int i = 0; i < m; ++i) {
        int a, b;
        char c;
        cin >> a >> b >> c;
        conditions[--a].second.emplace_back(--b, c);
    }
    doCondition(0);
    printf("Rejects\n");

    return 0;
}