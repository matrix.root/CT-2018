#include <iostream>
#include <vector>
#include <set>

#define TASK "problem2"

using namespace std;

string word;
vector<pair<bool, vector<pair<int, char>>>> conditions;

void accepts() {
    printf("Accepts\n");
    exit(0);
}

void add(int &condition, char &symbol, set<int> &toAdd) {
    for (auto next : conditions[condition].second) {
        if (next.second == symbol) {
            toAdd.insert(next.first);
        }
    }
}

void doCondition() {
    set<int> currentConditions;
    currentConditions.insert(0);
    for (auto symbol : word) {
        set<int> newConditions;
        for (auto condition : currentConditions) {
            add(condition, symbol, newConditions);
        }
        currentConditions = newConditions;
    }
    for (auto i : currentConditions) {
        if (conditions[i].first) {
            accepts();
        }
    }
}

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    int n, m, k;
    cin >> word >> n >> m >> k;
    conditions.resize(static_cast<unsigned long>(n));
    for (int i = 0; i < k; ++i) {
        int number;
        cin >> number;
        conditions[--number].first = true;
    }
    for (int i = 0; i < m; ++i) {
        int a, b;
        char c;
        cin >> a >> b >> c;
        conditions[--a].second.emplace_back(--b, c);
    }
    doCondition();
    printf("Rejects\n");

    return 0;
}